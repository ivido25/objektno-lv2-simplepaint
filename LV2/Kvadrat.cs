﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LV2
{
    class Kvadrat : Linija
    {
        public Kvadrat(GrafObj objekt, Point koordinata2) : base(objekt, koordinata2)
        {
            this.koordinata2 = koordinata2;
        }
        public override void DrawGrafObj(Graphics grafika)
        {
            Rectangle kvadrat = new Rectangle(koordinata.X, koordinata.Y, koordinata2.X - koordinata.X, koordinata2.X - koordinata.X);
            grafika.DrawRectangle(new Pen(boja, 1), kvadrat);
        }
    }
}
