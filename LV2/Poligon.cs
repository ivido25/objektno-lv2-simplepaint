﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LV2
{
    class Poligon : GrafObj
    {
        public Point[] koordinate = new Point[100];

        public Poligon(GrafObj objekt, Point[] koordinate) : base(objekt)
        {
            this.koordinate = koordinate;
        }
        public override void DrawGrafObj(Graphics grafika)
        {
            grafika.DrawPolygon(new Pen(boja, 1), koordinate);
        }
    }
}
