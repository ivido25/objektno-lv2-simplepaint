﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LV2
{
    class Linija : GrafObj
    {
        public Point koordinata2;

        public Linija(GrafObj objekt, Point koordinata2) : base(objekt)
        {
            this.koordinata2 = koordinata2;
        }

        public override void DrawGrafObj(Graphics grafika)
        {
            grafika.DrawLine(new Pen(boja, 1), koordinata, koordinata2);
        }
    }
}
