﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LV2
{
    class GrafObj
    {
        public Color boja;
        public Point koordinata;

        public void SetColor(Color boja)
        {
            this.boja = boja;
        }

        public Color GetColor()
        {
            return boja;
        }

        public GrafObj(GrafObj objekt)
        {
            boja = objekt.boja;
            koordinata = objekt.koordinata;
        }

        public GrafObj(Color boja, Point koordinata)
        {
            this.boja = boja;
            this.koordinata = koordinata;
        }
        virtual public void DrawGrafObj(Graphics grafika) { }
    }
}
