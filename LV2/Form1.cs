﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV2
{
    public partial class Form1 : Form
    {

        Point koordinata1, koordinata2;
        Point[] koordinate;

        Color boja;
        int i = 0;

        public Form1()
        {
            InitializeComponent();

            koordinate = new Point[20];
            koordinata1 = new Point();
            koordinata2 = new Point();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && rPoligon.Checked == false)
            {
                koordinata1 = e.Location;
            }
            if (e.Button == MouseButtons.Left && rPoligon.Checked == true)
            {
                do
                {
                    koordinate[i] = e.Location;
                    i++;
                } while (e.Button == MouseButtons.Right || i > 20);
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            Graphics grafika = this.CreateGraphics();
            odabranaBoja();
            GrafObj objekt = new GrafObj(boja, koordinata1);
            if (e.Button == MouseButtons.Left && rPoligon.Checked == false)
            {
                koordinata2 = e.Location;
            }
            if (rLinija.Checked == true)
            {
                Linija linija = new Linija(objekt, koordinata2);
                linija.DrawGrafObj(grafika);
            }
            if (rKvadrat.Checked == true)
            {
                Kvadrat kvadrat = new Kvadrat(objekt, koordinata2);
                kvadrat.DrawGrafObj(grafika);
            }
            if (rKruznica.Checked == true)
            {
                Kruznica kruznica = new Kruznica(objekt, koordinata2);
                kruznica.DrawGrafObj(grafika);
            }
            if (rElipsa.Checked == true)
            {
                Elipsa elipsa = new Elipsa(objekt, koordinata2);
                elipsa.DrawGrafObj(grafika);
            }
            if (rPoligon.Checked == true)
            {
                if (e.Button == MouseButtons.Right)
                {
                    koordinate[i] = koordinate[0];
                    for (; i < 19; i++)
                    {
                        koordinate[i + 1] = koordinate[0];
                    }
                    Poligon poligon = new Poligon(objekt, koordinate);
                    poligon.DrawGrafObj(grafika);
                    i = 0;
                    koordinate = new Point[20];
                }
            }
        }

        public void odabranaBoja()
        {
            if (rCrna.Checked == true)
            {
                boja = Color.Black;
            }
            if (rCrvena.Checked == true)
            {
                boja = Color.Red;
            }
            if (rPlava.Checked == true)
            {
                boja = Color.Blue;
            }
        }
    }
}
