﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LV2
{
    class Elipsa : Kruznica
    {
        public Elipsa(GrafObj objekt, Point koordinata2):base(objekt, koordinata2)
        {
            polumjerX = Math.Abs(koordinata.X - koordinata2.X);
            polumjerY = Math.Abs(koordinata.Y - koordinata2.Y);
        }

        public override void DrawGrafObj(Graphics grafika)
        {
            grafika.DrawEllipse(new Pen(boja, 1), koordinata.X - polumjerX, koordinata.Y - polumjerY, 2*polumjerX, 2*polumjerY);
        }
    }
}
