﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace LV2
{
    class Kruznica : GrafObj
    {
        public float polumjerY, polumjerX, polumjer=0;
        public Point koordinata2;

        public Kruznica(GrafObj objekt, Point koordinata2) : base(objekt)
        {
            this.koordinata2 = koordinata2;
           
           
                polumjerX = Math.Abs(koordinata.X - koordinata2.X);
                polumjerY = Math.Abs(koordinata.Y - koordinata2.Y);
            if (polumjerX > polumjerY)
            {
                polumjer = polumjerX;
            }
            else polumjer = polumjerY;
           
        }

        public override void DrawGrafObj(Graphics grafika)
        {
            grafika.DrawEllipse(new Pen(boja, 1), koordinata.X - polumjer, koordinata.Y - polumjer, 2 * polumjer, 2 * polumjer);
        }
    }
}
