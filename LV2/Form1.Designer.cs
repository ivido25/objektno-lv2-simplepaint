﻿namespace LV2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gB_objekti = new System.Windows.Forms.GroupBox();
            this.rKvadrat = new System.Windows.Forms.RadioButton();
            this.rPoligon = new System.Windows.Forms.RadioButton();
            this.rLinija = new System.Windows.Forms.RadioButton();
            this.rKruznica = new System.Windows.Forms.RadioButton();
            this.rElipsa = new System.Windows.Forms.RadioButton();
            this.gB_Boja = new System.Windows.Forms.GroupBox();
            this.rPlava = new System.Windows.Forms.RadioButton();
            this.rCrvena = new System.Windows.Forms.RadioButton();
            this.rCrna = new System.Windows.Forms.RadioButton();
            this.gB_objekti.SuspendLayout();
            this.gB_Boja.SuspendLayout();
            this.SuspendLayout();
            // 
            // gB_objekti
            // 
            this.gB_objekti.Controls.Add(this.rKvadrat);
            this.gB_objekti.Controls.Add(this.rPoligon);
            this.gB_objekti.Controls.Add(this.rLinija);
            this.gB_objekti.Controls.Add(this.rKruznica);
            this.gB_objekti.Controls.Add(this.rElipsa);
            this.gB_objekti.Location = new System.Drawing.Point(1069, 15);
            this.gB_objekti.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gB_objekti.Name = "gB_objekti";
            this.gB_objekti.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gB_objekti.Size = new System.Drawing.Size(133, 167);
            this.gB_objekti.TabIndex = 0;
            this.gB_objekti.TabStop = false;
            this.gB_objekti.Text = "Graf. Objekti";
            // 
            // rKvadrat
            // 
            this.rKvadrat.AutoSize = true;
            this.rKvadrat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rKvadrat.Location = new System.Drawing.Point(9, 138);
            this.rKvadrat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rKvadrat.Name = "rKvadrat";
            this.rKvadrat.Size = new System.Drawing.Size(78, 21);
            this.rKvadrat.TabIndex = 4;
            this.rKvadrat.TabStop = true;
            this.rKvadrat.Text = "Kvadrat";
            this.rKvadrat.UseVisualStyleBackColor = true;
            // 
            // rPoligon
            // 
            this.rPoligon.AutoSize = true;
            this.rPoligon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rPoligon.Location = new System.Drawing.Point(9, 110);
            this.rPoligon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rPoligon.Name = "rPoligon";
            this.rPoligon.Size = new System.Drawing.Size(76, 21);
            this.rPoligon.TabIndex = 3;
            this.rPoligon.TabStop = true;
            this.rPoligon.Text = "Poligon";
            this.rPoligon.UseVisualStyleBackColor = true;
            // 
            // rLinija
            // 
            this.rLinija.AutoSize = true;
            this.rLinija.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rLinija.Location = new System.Drawing.Point(9, 81);
            this.rLinija.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rLinija.Name = "rLinija";
            this.rLinija.Size = new System.Drawing.Size(62, 21);
            this.rLinija.TabIndex = 2;
            this.rLinija.TabStop = true;
            this.rLinija.Text = "Linija";
            this.rLinija.UseVisualStyleBackColor = true;
            // 
            // rKruznica
            // 
            this.rKruznica.AutoSize = true;
            this.rKruznica.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rKruznica.Location = new System.Drawing.Point(9, 53);
            this.rKruznica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rKruznica.Name = "rKruznica";
            this.rKruznica.Size = new System.Drawing.Size(84, 21);
            this.rKruznica.TabIndex = 1;
            this.rKruznica.TabStop = true;
            this.rKruznica.Text = "Kruznica";
            this.rKruznica.UseVisualStyleBackColor = true;
            // 
            // rElipsa
            // 
            this.rElipsa.AutoSize = true;
            this.rElipsa.Checked = true;
            this.rElipsa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rElipsa.Location = new System.Drawing.Point(9, 25);
            this.rElipsa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rElipsa.Name = "rElipsa";
            this.rElipsa.Size = new System.Drawing.Size(67, 21);
            this.rElipsa.TabIndex = 0;
            this.rElipsa.TabStop = true;
            this.rElipsa.Text = "Elipsa";
            this.rElipsa.UseVisualStyleBackColor = true;
            // 
            // gB_Boja
            // 
            this.gB_Boja.Controls.Add(this.rPlava);
            this.gB_Boja.Controls.Add(this.rCrvena);
            this.gB_Boja.Controls.Add(this.rCrna);
            this.gB_Boja.Location = new System.Drawing.Point(1069, 190);
            this.gB_Boja.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gB_Boja.Name = "gB_Boja";
            this.gB_Boja.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gB_Boja.Size = new System.Drawing.Size(133, 110);
            this.gB_Boja.TabIndex = 1;
            this.gB_Boja.TabStop = false;
            this.gB_Boja.Text = "Boja";
            // 
            // rPlava
            // 
            this.rPlava.AutoSize = true;
            this.rPlava.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rPlava.Location = new System.Drawing.Point(8, 80);
            this.rPlava.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rPlava.Name = "rPlava";
            this.rPlava.Size = new System.Drawing.Size(64, 21);
            this.rPlava.TabIndex = 3;
            this.rPlava.TabStop = true;
            this.rPlava.Text = "Plava";
            this.rPlava.UseVisualStyleBackColor = true;
            // 
            // rCrvena
            // 
            this.rCrvena.AutoSize = true;
            this.rCrvena.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rCrvena.Location = new System.Drawing.Point(8, 52);
            this.rCrvena.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rCrvena.Name = "rCrvena";
            this.rCrvena.Size = new System.Drawing.Size(74, 21);
            this.rCrvena.TabIndex = 2;
            this.rCrvena.TabStop = true;
            this.rCrvena.Text = "Crvena";
            this.rCrvena.UseVisualStyleBackColor = true;
            // 
            // rCrna
            // 
            this.rCrna.AutoSize = true;
            this.rCrna.Checked = true;
            this.rCrna.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rCrna.Location = new System.Drawing.Point(8, 23);
            this.rCrna.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rCrna.Name = "rCrna";
            this.rCrna.Size = new System.Drawing.Size(59, 21);
            this.rCrna.TabIndex = 1;
            this.rCrna.TabStop = true;
            this.rCrna.Text = "Crna";
            this.rCrna.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1218, 661);
            this.Controls.Add(this.gB_Boja);
            this.Controls.Add(this.gB_objekti);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.gB_objekti.ResumeLayout(false);
            this.gB_objekti.PerformLayout();
            this.gB_Boja.ResumeLayout(false);
            this.gB_Boja.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gB_objekti;
        private System.Windows.Forms.RadioButton rKvadrat;
        private System.Windows.Forms.RadioButton rPoligon;
        private System.Windows.Forms.RadioButton rLinija;
        private System.Windows.Forms.RadioButton rKruznica;
        private System.Windows.Forms.RadioButton rElipsa;
        private System.Windows.Forms.GroupBox gB_Boja;
        private System.Windows.Forms.RadioButton rPlava;
        private System.Windows.Forms.RadioButton rCrvena;
        private System.Windows.Forms.RadioButton rCrna;
    }
}

